/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alexander Velazquez
 *
 * Created on February 29, 2020, 8:57 AM
 */

#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    cout << "Hello, my name is Hal!" << endl;
    cout << "What is your name?" << endl;
    
    string name;
    cout << "Please enter a name: ";
    cin >> name;
    cout << "Hello," << name; 
    cout << "! Nice to meet you!";
    return 0;
}

