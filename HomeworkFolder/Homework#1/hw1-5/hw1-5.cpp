/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alexander Velazquez
 *
 * Created on March 7, 2020, 11:52 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string yourname = "Alexander Velazquez",
            yourtelephonenumber = "951-555-5555",
            yourmajor = "Aerospace Engineering";
    
    cout << "Name:" << yourname << "\n" << 
            "Cell-phone: " << 
            yourtelephonenumber << "\n" << "Major: " <<
            yourmajor;

    return 0;
}

