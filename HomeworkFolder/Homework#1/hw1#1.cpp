/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alexander Velazquez
 *
 * Created on February 29, 2020, 10:22 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main() {
    int firstNumber, secondNumber, sumOfTwoNumbers;
    
    cout << " Enter the numbers 50 and 100 :";
    
    cin >> firstNumber >> secondNumber; 
    
    sumOfTwoNumbers = firstNumber + secondNumber;
    cout << firstNumber << " + " << secondNumber << " = " << sumOfTwoNumbers;
    
    return 0;
}

