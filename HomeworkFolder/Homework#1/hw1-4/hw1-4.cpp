/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alex
 *
 * Created on March 7, 2020, 11:43 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
    const float travelmiles = 375;
    const float totalgas = 15;
    
    float mpg = travelmiles / totalgas;
    
    cout << mpg << "MPG";
    return 0;
}

