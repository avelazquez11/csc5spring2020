/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alex
 *
 * Created on March 7, 2020, 11:35 AM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    double mealCost = 88.67;
    double tax, tip, total;
    
    tax = 0.0675 * mealCost;
    tip = 0.2 * (mealCost + tax);
    total = mealCost + tax+ tip;
    
    cout << "Meal Cost is $" << mealCost << endl;
    cout << "Tax amount is $" << tax << endl;
    cout << "Tip amount is $" << tip << endl;
    cout << "Total Bill is $" << total << endl;
    return 0;
}

