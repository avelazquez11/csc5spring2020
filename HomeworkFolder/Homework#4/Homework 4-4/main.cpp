/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Alexander Velazquez
 *
 * Created on March 28, 2020, 12:05 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
double const LITER = 0.264172;
double milesPerGallon(int mi, int lt);

/*
 * 
 */
int main() 
{
    char options;
    int lt, mi;
    do
    { 
        cout << " Enter the liters of gasoline: ";
        cin >> lt;
        cout << "Enter the number of miles traveled: ";
        cin >> mi;
        cout << "MPG: " << milesPerGallon(mi, lt) << endl;
        cout << "To continue, then enter 'Y':";
        cin >> options;
    } while (options == 'y' || options == 'Y');

    return 0;
}
double milesPerGallon(int m, int l)
{
    double gallons;
    gallons = LITER * l;
    return (m/gallons);
}
